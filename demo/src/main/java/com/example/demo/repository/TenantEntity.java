package com.example.demo.repository;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.TimeZone;
import java.util.UUID;

@Entity
@Table(name = "TENANT")
@Getter
@Setter
public class TenantEntity implements Serializable {
    @Id
    @Type(type = "uuid-char")
    @Column(name = "ID")
    private UUID id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "MODIFIED_ON", nullable = false)
    @LastModifiedDate
    protected OffsetDateTime modifiedOn;

    @Column(name = "WITH_TIMEZONE", nullable = false)
    @LastModifiedDate
    protected OffsetDateTime timeZOneDate;

    @PrePersist
    public void prePersist(){
            modifiedOn = OffsetDateTime.now();
            timeZOneDate = OffsetDateTime.now();
    }

    @PreUpdate
    public void preUpdate() {
        modifiedOn = OffsetDateTime.now();
        timeZOneDate = OffsetDateTime.now();
    }


    public TenantEntity() {
    }

    public static TenantEntity buildTenant(){
        TenantEntity tenantEntity = new TenantEntity();
        tenantEntity.id = UUID.randomUUID();
        tenantEntity.name = UUID.randomUUID().toString();


        return tenantEntity;
    }

    public static TenantEntity buildTenantForHawaii(){
        TenantEntity tenantEntity = new TenantEntity();
        tenantEntity.id = UUID.randomUUID();
        tenantEntity.name = "Hawaii_" + UUID.randomUUID().toString();

        //this will not work since the local server is set to UTC
        //so it's all good
        tenantEntity.modifiedOn = OffsetDateTime.now(ZoneId.of("US/Hawaii"));
        tenantEntity.timeZOneDate = OffsetDateTime.now(ZoneId.of("US/Hawaii"));

        return tenantEntity;
    }
}

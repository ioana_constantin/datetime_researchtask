package com.example.demo.controller;

import com.example.demo.repository.TenantEntity;
import com.example.demo.service.TenantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController("tenant")
public class TenantController {
    @Autowired
    private TenantService tenantService;

    @GetMapping(value = "/",consumes = MediaType.APPLICATION_JSON_VALUE)
    public List<TenantEntity> getTenants(){
        return tenantService.getTenants();
    }

    @PostMapping(value = "/", consumes = MediaType.APPLICATION_JSON_VALUE)
    public TenantEntity saveTenant(){
        return tenantService.saveTenant();
    }

    @PostMapping(value = "/hawaii", consumes = MediaType.APPLICATION_JSON_VALUE)
    public TenantEntity saveTenantForHawaii(){
        return tenantService.saveTenantForHawaii();
    }
}

package com.example.demo.config;

import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.TimeZone;

@Configuration
public class LocalConfig {

    @PostConstruct
    public void init() {

        //we need to set this in order to save only UTC timestamp
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        System.out.println(new Date());

    }
}

package com.example.demo.service;


import com.example.demo.repository.TenantEntity;
import com.example.demo.repository.TenantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class TenantService {
    @Autowired
    private TenantRepository tenantRepository;

    public TenantEntity saveTenant(){
        return tenantRepository.save(TenantEntity.buildTenant());
    }

    //this will not store the Hawaii timestamp because will be overridden by UTC
    public TenantEntity saveTenantForHawaii(){
        return tenantRepository.save(TenantEntity.buildTenantForHawaii());
    }

    public List<TenantEntity> getTenants(){
        return tenantRepository.findAll();
    }

}
